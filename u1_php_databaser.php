<?php

/**
 * ===== Ex. =====
 * Nedan är ett ex på kod som:
 * - Kopplar upp sig mot en databas
 * - Väljer databas
 * - Ställer en fråga
 * - Tar hand om resultatet
 *
 * Punkt 1 och 2 behöver man bara göra en gång
 * Punkt 3 och 4 kan man göra hur många som helst
 *
 *
 * 1 och 2 behöver man bara göra en gång, detta kan man med fördel ha i
 * en fil man tar include på. Man kan sätta chmod 700 på den filen så kan
 * ingen annan än ni själva läsa innehållet.
 *
 * 3 och 4 gör man ofta flera gånger per sida.
 */

include 'jf_select.php';

// 3. Ställ en fråga till databasen
// Skapa frågan som sträng
$query = "SELECT * FROM Names WHERE LastName LIKE '%" . $_GET['lname'] . "%'";
// Ställ frågan
$result = mysqli_query($link, $query);
echo mysqli_error($link);

// 4. Skriv ut svaret med hjälp av en while-loop
while ($row = mysqli_fetch_assoc($result)) {
  echo $row['FirstName'] . ' - ' . $row['LastName'] . '<br>';
}
