<?php

echo '<div class="jumbotron">
  <h1 class="display-4">Alla inlägg</h1>
  <p class="lead">Här kan ni se alla inlägg</p>
</div>';

echo '
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Login</th>
        <th scope="col">Namn</th>
        <th scope="col"><a href="?asc=' . (empty($_GET['asc']) ? "1" : "0") . '">Antal</a></th>
    </tr>
    </thead>
    <tbody>';

// Hämta upp alla användare.
$query = "SELECT *, join_users.user_id as user_id, count(join_items.item_id) as n FROM join_users 
LEFT JOIN join_items ON join_users.user_id = join_items.user_id 
GROUP BY join_users.user_id 
ORDER BY n " . (empty($_GET['asc']) ? 'DESC' : 'ASC') . ', Name';
$result = mysqli_query($link, $query);
while ($row = mysqli_fetch_assoc($result)) {
  echo '
    <tr>
      <th scope="row" >' . $row['user_id'] . '</th >
      <td><a href="?id=' . $row['user_id'] . '" >' . $row['login'] . '</a></td >
      <td>' . $row['name'] . '</td >
      <td>' . $row['n'] . '</td >
    </tr>';
}

echo '</tbody>
    </table>';
