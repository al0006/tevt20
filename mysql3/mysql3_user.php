<?php
if (isset($_GET['id'])) {
  $query = "SELECT * FROM join_users WHERE user_id = '" . $_GET['id'] . "'";
  $result = mysqli_query($link, $query);
  $row = mysqli_fetch_assoc($result);
  echo '<div class="jumbotron">
    <h1 class="display-4">' . $row['name'] . '</h1>
    <p class="lead">Här kan ni se alla inlägg gjort av ' . $row['login'] . '<hr class="my-4">
    <a class="btn btn-primary btn-lg" href="' . $_SERVER['PHP_SELF'] . '" role="button">Tillbaka</a>
  </div>';

  echo '
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Datum</th>
        <th scope="col">Item</th>
    </tr>
    </thead>
    <tbody>';

  // Hämta upp alla användare.
  $query = "SELECT * FROM join_items WHERE user_id = '" . $_GET['id'] . "' ORDER BY date";
  $result = mysqli_query($link, $query);
  while ($row = mysqli_fetch_assoc($result)) {
    echo '
    <tr>
        <th scope="row" >' . $row['item_id'] . '</th >
        <td>' . $row['date'] . '</td >
        <td>' . $row['item'] . '</td >
    </tr>';
  }

  echo '</tbody>
    </table>';
}
