<?php
/* Kör programmet i terminalen med:
php ex_cli.php
*/
/**
 * Detta är ett litet exempel på CLI-php
 */
// ställer in så att man ser alla fel utom NOTICE
error_reporting(E_ALL ^ E_NOTICE);
// rensa skärmen
system('clear');
// Utskrift från PHP
echo "Välkommen till denna testfil\n";
?>

    Här är inte PHP igång så detta kommer att skrivas ut ;-)

<?php
echo "Ange ett tal: ";
// Läsa in från tangetbordet.
$handle = fopen("php://stdin", "r");
$tal = (int) fgets($handle); // tvingar till heltal

// en styrande sats
if ($tal > 10) {
  echo "Du skrev in ett tal större än 10...\n\n";
}
else {
  echo "Du skrev in ett tal mindre en eller lika med 10...\n\n";
}

// en loop
$i = 0;
while ($i < $tal) {
  echo "\nNr. " . $i;
  $i++;
}
echo "\nSkriv något: ";
// För att läsa in en sträng utan radbrytning på slutet
$str = trim(fgets($handle));
echo $str;
