<?php
// 1. Koppla upp sig mot databasen
// server, login, password, database
$link = mysqli_connect("localhost", "lektion", "lektion", "lektion");

// 2. Säg till databasen att man vill ha ut resultatet i utf8
mysqli_set_charset($link, "utf8");

// Ta hand om data som kommer i $_POST
// om korrekt lägg in i databasen
if (!empty($_POST['item'])) {
  $query = "INSERT INTO `Items` (`ItemID`, `Item`, `Date`) 
    VALUES (NULL, '" . $_POST['item'] . "', NOW());";
  mysqli_query($link, $query);
}
include 'head.php';

echo '
    <form method="post">
        <input type="text" name="item">
        <input type="submit">
    </form>';

// Visa alla inlägg som finns i databasen.
$query = "SELECT * FROM Items ORDER BY Date desc";
$result = mysqli_query($link, $query);
while ($row = mysqli_fetch_assoc($result)) {
  echo $row['Item'] . ' gjort: ' . $row['Date'] . '<br>';
}

include 'foot.php';
